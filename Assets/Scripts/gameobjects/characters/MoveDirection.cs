﻿public enum MoveDirection {
    FORWARD, //Z+
    BACK, //Z-
    LEFT, //X-
    RIGHT, //X+
    STOP
}
