http://www.saitogames.com/hopmon/
и официальное описание

    HOPMON is a 3D arcade action game. There are 45 levels in total and to complete each level, Hopmon will have to collect all the crystals and place them on the warpzone where the star symbol is printed.
    This game is Shareware. Unregistered version is limited to the first 10 levels of play.

И немного о игре от меня:
Нашел я эту игру в сборнике «Игры для прекрасных дам», в котором содержалось довольно приличное количество игр и многие из них были очень увлекательными и интересными: это пиратская аркада Shipwreckers, необычная змейка NeedForEat, очень интересные игры на логику Lemmings Revolution, Sleep Walker, стратегия с уникальным управлением Majesty.
Суть игры Hopmon заключается в том, что Вы управляете персонажем, возможно животно-покемонного происхождения, и Вашей задачей является бегая по лабирину, собрать все кристаллы и отнести на стартовую точку. Жизнь у вас одна, и соответственно одно право на ошибку, иначе уровень придется начинать заного. Немаловажной частью геймплея является то, что чем больше набрал Ваш персонаж кристалов, тем тяжелее ему передвигаться и в этом случае некоторые препятствия невозможно будет пройти, либо необходимо будет приложить большие усилия.

Я вышел на связь с разработчиком и просто попросил разрешение о возможности сделать ремастеринг игры на Unity с последующим добавление различных фич. Это будет чисто фановый некоммерческий проект. Именно эти условия были 